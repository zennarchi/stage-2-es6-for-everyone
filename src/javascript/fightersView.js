import View from './view';
import FighterView from './fighterView';
import Modal from './modal';
import { fighterService } from './services/fightersService';
import Fight from './fight';
import Fighter from './fighter';

class FightersView extends View {
  constructor(fighters) {
    super();
    this.battleFighters = [];
    this.handleClick = this.handleFighterClick.bind(this);
    this.handleSave = this.handleFighterSave.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();

  static getFightersDetailsMap() {
    return fightersDetailsMap;
  }

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  showModal(id) {
    let fighter = this.fightersDetailsMap.get(id);
    let modal = new Modal(fighter, this.handleSave);

    let rootElement = document.getElementById('root');
    rootElement.appendChild(modal.element);
    modal.element.style.display = "block";

  }
  checkFight() {
    const foundElements = document.querySelectorAll('input[type="checkbox"]:checked');
    if (foundElements.length === 2) {
      //console.log(foundElements[0].value);
      this.battleFighters[0] = foundElements[0].value;
      this.battleFighters[1] = foundElements[1].value;
      console.log(this.battleFighters);
      this.showBattleBtn();
    } else {
      console.log('bad');
    }
  }
  fight() {
    const fighterObj1 = this.fightersDetailsMap.get(this.battleFighters[0]);
    var fighter1 = new Fighter(fighterObj1.name, fighterObj1.health, fighterObj1.attack, fighterObj1.defense);

    const fighterObj2 = this.fightersDetailsMap.get(this.battleFighters[1]);
    var fighter2 = new Fighter(fighterObj2.name, fighterObj2.health, fighterObj2.attack, fighterObj2.defense);

    var firstHealth = document.getElementsByClassName('health')[0];
    var secondHealth = document.getElementsByClassName('health')[1];

    var startFirstHealth = fighter1.health;
    var startSecondHealth = fighter2.health;

    console.log(document.getElementsByClassName('health')[0]);

    var intervalId = setInterval(async function () {
      if (fighter1.health > 0 && fighter2.health > 0) {
        let demage = fighter2.getHitPower() - fighter1.getBlockPower();
        if (demage < 0) {
          demage = 0;
        }
        fighter1.health = fighter1.health - demage;
        console.log(fighter1.name + ' : ' + fighter1.health);
        firstHealth.innerText = Math.round((fighter1.health / startFirstHealth) * 100 * 100 / 100) + '%';

        //second part of a fight
        demage = fighter1.getHitPower() - fighter2.getBlockPower();
        if (demage < 0) {
          demage = 0;
        }
        fighter2.health = fighter2.health - demage;

        console.log(fighter2.name + ' : ' + fighter2.health);
        secondHealth.innerText = Math.round((fighter2.health / startSecondHealth) * 100 * 100 / 100) + '%';
      } else {
        clearInterval(intervalId);
        if (fighter1.health < 0) {
          alert(fighter2.name + ' Wins!');
        } else {
          alert(fighter1.name + ' Wins!');
        }
      }

    }, 500)
  }


  showBattleBtn() {
    const fightWrapperElement = document.getElementById('fight-wrapper');
    //console.log(fightWrapper);
    const firstFighterHealth = this.createElement({ tagName: 'h3', className: 'health' });
    const fightBtn = this.createElement({ tagName: 'button', className: 'btn' });
    const secondFighterHealth = this.createElement({ tagName: 'h3', className: 'health' });
    fightBtn.innerText = 'Battle !';
    fightBtn.addEventListener('click', event => this.fight());
    fightWrapperElement.append(firstFighterHealth, fightBtn, secondFighterHealth);

  }

  async handleFighterClick(event, fighter) {
    console.log(this.fightersDetailsMap);
    if (!this.fightersDetailsMap.has(fighter._id)) {
      const fighterDetails = await fighterService.getFighterDetails(fighter._id);
      this.fightersDetailsMap.set(fighter._id, fighterDetails);

    }
    this.showModal(fighter._id);
    this.checkFight();

  }

  handleFighterSave(event, fighter, health, attack, defense) {
    let oldDetails = this.fightersDetailsMap.get(fighter._id);
    oldDetails.health = +document.getElementById('health').value;
    oldDetails.attack = +document.getElementById('attack').value;
    oldDetails.defense = +document.getElementById('defense').value;
    this.fightersDetailsMap.set(fighter._id, oldDetails);
    console.log(this.fightersDetailsMap);
  }



}

export default FightersView;